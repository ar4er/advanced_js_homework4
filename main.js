const root = document.getElementById("root");

function createElement(tagName, className) {
  const element = document.createElement(tagName);
  element.className = className;
  return element;
}

let response = fetch("https://ajax.test-danit.com/api/swapi/films", {
  method: "GET",
})
  .then((answer) => answer.json())
  .then((filmsArray) => {
    const sortedFilmsArray = [...filmsArray].sort(
      (a, b) => a.episodeId - b.episodeId
    );

    sortedFilmsArray.map((film) => {
      const list = createElement("ul", "film");
      root.append(list);
      const episodeName = createElement("li", "episode-name");
      episodeName.innerText = `Епізод ${film.episodeId}. ${film.name}`;
      const episodeDescription = createElement("li", "episode-description");
      episodeDescription.innerText = film.openingCrawl;
      const characters = createElement("li", "characters");
      const charactersList = createElement("ul", "characters-list");
      charactersList.insertAdjacentHTML(
        "beforeend",
        `<li>Список персонажів:</li>`
      );
      characters.append(charactersList);
      const loadingAnimation = createElement(
        "div",
        `animation animation-${film.episodeId}`
      );
      list.append(
        episodeName,
        loadingAnimation,
        characters,
        episodeDescription
      );

      for (character of film.characters) {
        fetch(character, {
          method: "GET",
        })
          .then((answer) => {
            if (answer.status >= 200 && answer.status < 300)
              loadingAnimation.remove();
            return answer.json();
          })
          .then((done) =>
            charactersList.insertAdjacentHTML(
              "beforeend",
              `<li>${done.name}</li>`
            )
          );
      }
    });
  });
